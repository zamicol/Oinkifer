//Words to search and replace.
//Some words could be serilized without spaces. Use
var wordList = {
	"Donald": "Oinkifer",
	"Trump": "Tiny Hands",
	"great": "oink",
	"again": "oinkoink",
	"vote": "squeeel",
	"Hillary": "Farmer",
	"Clinton ": "Old Macdonald",
	"Rosie O'Donnell": "Spoon Lady",
	"million": "MUUUUNEY",
	"GOP": "slaughter house",
	"Dem": "free range rabbit",
	"tax": "kosher bacon",
	"fact": "hoggin'",
	"election": "pig show",
	"ISIS": "feed trough",
	"terror": "swine",
	"loser": "third party candidate",
	"the media": "anyone with a brain",
	"children": "piglets",
	"republican": "hamlet",
	"leader": "head hog"
			//manure
			//hogwash
};

var lowList = {}


//Check to see if Trump is mentioned.
var r = /trump/ig;
var s = r.exec(document.body.textContent);

//Trump was mentioned.  Oinkify!
if (s) {
	console.log("Trump found. Oinkify!");

	var regex = getRegex(wordList);

	//Replace only text nodes
	textReplace(document.body, regex);
}


//Change only text appearing on the page, not scripts, links, or other code.
function textReplace(node, regex) {
	if (node.nodeType === 3) { // 3 is a text node
		node.nodeValue = node.nodeValue.replace(regex, function (match) {
			s = match.toLowerCase();
			return lowList[s];
		});
		return;
	}

	if (node.nodeType === 1) { // 1 is an element
		for (var i = 0; i < node.childNodes.length; i++) {
			textReplace(node.childNodes[i], regex);
		}
	}
}


//Construct the final regex from an intial word list.
function getRegex(list) {
	//Make sure all our keys are lower case
	for (var key in list) {
		s = key.toString().toLowerCase();
		console.log(s);
		lowList[s] = wordList[key];
	}

	console.log(lowList);

	//Some Keys could be serialized without spaces.
	//In that case, we want to make sure that our replacement string (the value)
	//also lacks spaces to maintain conformity.
	var spaceReg = /\s/ig;

	for (var key in list) {
		var s = spaceReg.exec(lowList[key]);

		if (s) {
			str = lowList[key].replace(/\s/g, '');
			//Value with no spaces has key with no spaces.
			lowList[key] = str;

			//Value with spaces must have key with space
			//either preceeding or following. (mid sentence)
			//Construct regex that looks like this:
			//Example:    '\sKey|Key\s'
			spacedKey = "/\s" + key + "|" + key + "/\s";
			lowList[spacedKey] = lowList[key];
		}
	}
	console.log(lowList);


	//fruits.unshift

	//Put all the words into our regex.
	var regex = [];
	for (var key in lowList) {
		regex.push(key);
	}
	var regex = new RegExp(regex.join('|'), "ig");
	console.log(regex)

	return regex;
	//

}


